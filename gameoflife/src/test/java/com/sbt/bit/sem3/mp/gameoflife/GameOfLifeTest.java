package com.sbt.bit.sem3.mp.gameoflife;

import com.sbt.bit.sem3.mp.gameoflife.testsupport.Utils;

import java.io.FileNotFoundException;

import static org.junit.Assert.assertArrayEquals;

/**
 * @author Vasyukevich Andrey
 * @since 17.12.2017
 */
public abstract class GameOfLifeTest {
    protected GameOfLife gameOfLife;

    protected void testOneGame(String inputFile, String expectedOutputFile) throws FileNotFoundException {
        GameOfLifeConfig config = Utils.readGameOfLifeConfig(inputFile);
        int[][] result = gameOfLife.play(config);
        int[][] expected = Utils.readGameField(config.getNRows(), config.getNCols(), expectedOutputFile);
        assertArrayEquals(expected, result);
    }
}
