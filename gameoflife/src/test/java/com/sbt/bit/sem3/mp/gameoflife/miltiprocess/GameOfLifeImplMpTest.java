package com.sbt.bit.sem3.mp.gameoflife.miltiprocess;

import com.sbt.bit.sem3.mp.gameoflife.GameOfLifeTest;
import com.sbt.bit.sem3.mp.gameoflife.multiprocess.GameOfLifeImplMp;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Vasyukevich Andrey
 * @since 17.12.2017
 */
public class GameOfLifeImplMpTest extends GameOfLifeTest {

    @Before
    public void before() {
        gameOfLife = new GameOfLifeImplMp(2, 2, 100);
    }

    @Test
    public void testGame() throws Exception {
        testOneGame("resources/input.txt", "resources/output.txt");
    }

    @Test
    public void testGame100() throws Exception {
        testOneGame("resources/input100.txt", "resources/output100.txt");
    }

    @Test
    public void testGame100_10000() throws Exception {
        testOneGame("resources/input100_10000.txt", "resources/output100_10000.txt");
    }

    @Test
    public void testGame1000() throws Exception {
        testOneGame("resources/input1000.txt", "resources/output1000.txt");
    }

    @Test
    public void testGame1000_1000() throws Exception {
        testOneGame("resources/input1000_1000.txt", "resources/output1000_1000.txt");
    }

    @Test
    public void testGame10000_100() throws Exception {
        testOneGame("resources/input10000_100.txt", "resources/output10000_100.txt");
    }

    @Test
    public void testGame10000() throws Exception {
        testOneGame("resources/input10000.txt", "resources/output10000.txt");
    }
}
