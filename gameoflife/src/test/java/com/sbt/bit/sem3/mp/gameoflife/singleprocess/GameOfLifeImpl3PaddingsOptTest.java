package com.sbt.bit.sem3.mp.gameoflife.singleprocess;

import com.sbt.bit.sem3.mp.gameoflife.GameOfLifeTest;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Vasyukevich Andrey
 * @since 17.12.2017
 */
public class GameOfLifeImpl3PaddingsOptTest extends GameOfLifeTest {

    @Before
    public void before() {
        gameOfLife = new GameOfLifeImpl3PaddingsOpt(1, 1);
    }

    @Test
    public void testGame() throws Exception {
        testOneGame("resources/input.txt", "resources/output.txt");
    }

    @Test
    public void testGame100() throws Exception {
        testOneGame("resources/input100.txt", "resources/output100.txt");
    }

    @Test
    public void testGame100_10000() throws Exception {
        testOneGame("resources/input100_10000.txt", "resources/output100_10000.txt");
    }

    @Test
    public void testGame1000() throws Exception {
        testOneGame("resources/input1000.txt", "resources/output1000.txt");
    }

    @Test
    public void testGame1000_1000() throws Exception {
        testOneGame("resources/input1000_1000.txt", "resources/output1000_1000.txt");
    }

    @Test
    public void testGame10000_100() throws Exception {
        testOneGame("resources/input10000_100.txt", "resources/output10000_100.txt");
    }

    @Test
    public void testGame10000() throws Exception {
        testOneGame("resources/input10000.txt", "resources/output10000.txt");
    }
}
