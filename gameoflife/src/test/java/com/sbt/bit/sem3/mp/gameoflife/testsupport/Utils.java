package com.sbt.bit.sem3.mp.gameoflife.testsupport;

import com.sbt.bit.sem3.mp.gameoflife.GameOfLifeConfig;

import java.io.*;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * @author Vasyukevich Andrey
 * @since 17.12.2017
 */
public class Utils {
    public static GameOfLifeConfig readGameOfLifeConfig(String fileName) {
        try (InputStream inputStream = new FileInputStream(fileName)) {
            Scanner scanner = new Scanner(inputStream);
            int n = scanner.nextInt();
            int nGens = scanner.nextInt();
            scanner.nextLine();
            int[][] field = readGameField(n, n, scanner);
            return new GameOfLifeConfig(n, n, nGens, field);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int[][] readGameField(int nRows, int nCols, String fileName) {
        try (InputStream inputStream = new FileInputStream(fileName)) {
            Scanner scanner = new Scanner(inputStream);
            return readGameField(nRows, nCols, scanner);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int[][] readGameField(int nRows, int nCols, Scanner scanner) {
        int[][] field = new int[nRows][nCols];
        for (int i = 0; i < nRows; ++i) {
            String line = scanner.nextLine();
            for (int j = 0; j < nCols; ++j) {
                field[i][j] = Character.getNumericValue(line.charAt(j));
            }
        }
        return field;
    }

    public static void writeTestInput(int n, int nGens, int[][] gameField, String fileName) {
        try (PrintStream printStream = new PrintStream(fileName)) {
            printStream.format("%d %d\n", n, nGens);
            writeGameField(n, gameField, printStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeTestOutput(int n, int[][] gameField, String fileName) {
        try (PrintStream printStream = new PrintStream(fileName)) {
            writeGameField(n, gameField, printStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeGameField(int n, int[][] gameField, PrintStream printStream) {
        for (int i = 0; i < n; ++i) {
            String line = Arrays.stream(gameField[i]).mapToObj(Integer::toString).collect(Collectors.joining());
            printStream.println(line);
        }
    }
}
