package com.sbt.bit.sem3.mp.gameoflife.testsupport;

import com.sbt.bit.sem3.mp.gameoflife.GameOfLife;
import com.sbt.bit.sem3.mp.gameoflife.GameOfLifeConfig;
import com.sbt.bit.sem3.mp.gameoflife.singleprocess.GameOfLifeImpl4PaddingsOptMemSwap;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 * @author Vasyukevich Andrey
 * @since 18.12.2017
 */
@Ignore
public class TestGenerator {
    private GameOfLife gameOfLife;

    @Before
    public void before() {
        gameOfLife = new GameOfLifeImpl4PaddingsOptMemSwap(1, 1);
    }

    @Test
    public void generateTest100_10000() throws Exception {
        generateTest("resources/input100.txt", 10000,"resources/input100_10000.txt", "resources/output100_10000.txt");
    }

    @Test
    public void generateTest1000_1000() throws Exception {
        generateTest("resources/input1000.txt", 1000,"resources/input1000_1000.txt", "resources/output1000_1000.txt");
    }

    @Test
    public void generateTest10000_100() throws Exception {
        generateTest("resources/input10000.txt", 100,"resources/input10000_100.txt", "resources/output10000_100.txt");
    }

    public void generateTest(String inputFile, int nGens, String newTestInputFile, String newTestOutputFile) {
        GameOfLifeConfig config = Utils.readGameOfLifeConfig(inputFile);
        assertNotNull(config);

        int n = config.getNCols();
        int[][] field = config.getField();

        Utils.writeTestInput(n, nGens, field, newTestInputFile);

        int[][] result = gameOfLife.play(new GameOfLifeConfig(n, n, nGens, field));
        Utils.writeTestOutput(n, result, newTestOutputFile);
    }
}
