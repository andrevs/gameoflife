package com.sbt.bit.sem3.mp.gameoflife.multiprocess;

import java.util.concurrent.Callable;

/**
 * @author Vasyukevich Andrey
 * @since 17.12.2017
 */
public class GameOfLifeThreadTask implements Callable<ResultWrapper> {
    private final int firstRowInField;
    private final int vPad;
    private final int hPad;
    private final int nRows;
    private final int nCols;
    private final int nGens;

    private final int[][] fieldPart;

    public GameOfLifeThreadTask(int firstRow, int nRows, int nCols, int nGens, int[][] field) {
        this.firstRowInField = firstRow;
        this.vPad = nGens;
        this.hPad = 1;
        this.nRows = nRows;
        this.nCols = nCols;
        this.nGens = nGens;
        this.fieldPart = new int[vPad + nRows + vPad][hPad + nCols + hPad];
        for (int i = 0; i < vPad + nRows + vPad; ++i) {
            System.arraycopy(field[firstRow - vPad + i], 0, fieldPart[i], 0, hPad + nCols + hPad);
        }
    }

    @Override
    public ResultWrapper call() throws Exception {
        int[][] field = fieldPart;
        int[][] nextGenField = new int[vPad + nRows + vPad][hPad + nCols + hPad];

        for (int gen = 0; gen < nGens; ++gen) {
            int firstRow = gen + 1;
            int lastRow = vPad + nRows + vPad - firstRow;

            computeNextGenField(firstRow, lastRow, field, nextGenField);

            fillPaddings(firstRow, lastRow, field, nextGenField);

            int[][] tmp = field;
            field = nextGenField;
            nextGenField = tmp;
        }

        return new ResultWrapper(firstRowInField, field, vPad, nRows);
    }

    protected void computeNextGenField(int firstRow, int lastRow, int[][] field, int[][] nextGenField) {
        for (int i = firstRow; i < lastRow; ++i) {
            for (int j = hPad; j < hPad + nCols; ++j) {
                int nc = countNeighbours(i, j, field);
                nextGenField[i][j] = (nc == 3 || field[i][j] == 1 && nc == 2 ? 1 : 0);
            }
        }
    }

    protected void fillPaddings(int firstRow, int lastRow, int[][] field, int[][] nextGenField) {
        System.arraycopy(field[firstRow - 1], 0, nextGenField[firstRow - 1], 0, hPad + nCols + hPad);
        for (int i = firstRow; i < lastRow; ++i) {
            for (int j = hPad - 1; j >= 0; --j) {
                nextGenField[i][j] = nextGenField[i][j + nCols];
                nextGenField[i][hPad + nCols + hPad - 1 - j] = nextGenField[i][hPad + hPad - 1 - j];
            }
        }
        System.arraycopy(field[lastRow], 0, nextGenField[lastRow], 0, hPad + nCols + hPad);
    }

    protected int countNeighbours(int i, int j, int[][] field) {
        int nc = 0;
        nc += field[i - 1][j - 1] + field[i - 1][j] + field[i - 1][j + 1];
        nc += field[i][j - 1] + field[i][j + 1];
        nc += field[i + 1][j - 1] + field[i + 1][j] + field[i + 1][j + 1];
        return nc;
    }
}
