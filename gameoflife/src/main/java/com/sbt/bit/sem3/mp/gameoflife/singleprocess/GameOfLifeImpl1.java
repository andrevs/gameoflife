package com.sbt.bit.sem3.mp.gameoflife.singleprocess;

import com.sbt.bit.sem3.mp.gameoflife.GameOfLife;
import com.sbt.bit.sem3.mp.gameoflife.GameOfLifeConfig;

/**
 * @author Vasyukevich Andrey
 * @since 12.12.2017
 */
public class GameOfLifeImpl1 implements GameOfLife {
    @Override
    public int[][] play(GameOfLifeConfig config) {
        int[][] field = config.getField();
        for (int gen = 0; gen < config.getNGens(); ++gen) {
            field = computeNextGenField(config.getNRows(), config.getNCols(), field);
        }
        return field;
    }

    protected int[][] computeNextGenField(int nRows, int nCols, int[][] field) {
        int[][] nextGenField = new int[nRows][nCols];
        for (int i = 0; i < nRows; ++i) {
            for (int j = 0; j < nCols; ++j) {
                int nc = countNeighbours(i, j, nRows, nCols, field);
                nextGenField[i][j] = (nc == 3 || field[i][j] == 1 && nc == 2 ? 1 : 0);
            }
        }
        return nextGenField;
    }

    protected int countNeighbours(int i, int j, int nRows, int nCols, int[][] field) {
        int nc = 0;
        int[] d = {-1, 0, 1};
        for (int di : d) {
            for (int dj : d) {
                if (di != 0 || dj != 0) {
                    nc += field[(i + di + nRows) % nRows][(j + dj + nCols) % nCols];
                }
            }
        }
        return nc;
    }
}
