package com.sbt.bit.sem3.mp.gameoflife.multiprocess;

import com.sbt.bit.sem3.mp.gameoflife.GameOfLife;
import com.sbt.bit.sem3.mp.gameoflife.GameOfLifeConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author Vasyukevich Andrey
 * @since 17.12.2017
 */
public class GameOfLifeImplMp implements GameOfLife {
    protected final int vPad;
    protected final int hPad;
    protected final int nThreads;
    protected final int nParts;
    protected final int nGensPerIteration;
    protected final ExecutorService threadPool;

    protected int[][] field;

    public GameOfLifeImplMp(int nThreads, int nParts, int nGensPerIteration) {
        this.vPad = nGensPerIteration;
        this.hPad = 1;
        this.nThreads = nThreads;
        this.nParts = nParts;
        this.nGensPerIteration = nGensPerIteration;
        this.threadPool = Executors.newFixedThreadPool(nThreads);
    }

    @Override
    public int[][] play(GameOfLifeConfig config) {
        field = expand(config.getNRows(), config.getNCols(), config.getField());

        int curGen = 0;
        while (curGen < config.getNGens()) {
            int nGens = Math.min(nGensPerIteration, config.getNGens() - curGen);

            List<Future<ResultWrapper>> futures = submitTasks(config, nGens);

            try {
                writeFutureResults(config, futures);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

            fillPaddings(config.getNRows(), config.getNCols(), field);

            curGen += nGens;
        }

        return shrink(config.getNRows(), config.getNCols(), field);
    }

    protected List<Future<ResultWrapper>> submitTasks(GameOfLifeConfig config, int nGens) {
        List<Future<ResultWrapper>> futures = new ArrayList<>(nParts);

        int curPartFirstRow = vPad;
        int nRowsPerPart = (config.getNRows() + nParts - 1) / nParts;
        for (int i = 0; i < nParts; ++i) {
            if (curPartFirstRow + nRowsPerPart > vPad + config.getNRows()) {
                nRowsPerPart = vPad + config.getNRows() - curPartFirstRow;
            }
            GameOfLifeThreadTask task = new GameOfLifeThreadTask(curPartFirstRow, nRowsPerPart, config.getNCols(), nGens, field);
            futures.add(threadPool.submit(task));
            curPartFirstRow += nRowsPerPart;
        }

        return futures;
    }

    protected void writeFutureResults(GameOfLifeConfig config, List<Future<ResultWrapper>> futures) throws Exception {
        for (Future<ResultWrapper> future : futures) {
            ResultWrapper result = future.get();
            for (int i = 0; i < result.nRows; ++i) {
                System.arraycopy(result.fieldPart[result.firstRow + i], 0, field[result.firstRowInField + i], 0, hPad + config.getNCols() + hPad);
            }
        }
    }

    protected int[][] expand(int nRows, int nCols, int[][] field) {
        int[][] result = new int[vPad + nRows + vPad][hPad + nCols + hPad];
        for (int i = 0; i < nRows; ++i) {
            System.arraycopy(field[i], 0, result[i + vPad], hPad, nCols);
        }
        fillPaddings(nRows, nCols, result);
        return result;
    }

    protected int[][] shrink(int nRows, int nCols, int[][] field) {
        int[][] result = new int[nRows][nCols];
        for (int i = 0; i < nRows; ++i) {
            System.arraycopy(field[i + vPad], hPad, result[i], 0, nCols);
        }
        return result;
    }

    protected void fillPaddings(int nRows, int nCols, int[][] field) {
        for (int i = vPad - 1; i >= 0; --i) {
            System.arraycopy(field[i + nRows], hPad, field[i], hPad, nCols);
            System.arraycopy(field[vPad + vPad - 1 - i], hPad, field[vPad + nRows + vPad - 1 - i], hPad, nCols);
        }
        for (int i = 0; i < vPad + nRows + vPad; ++i) {
            for (int j = hPad - 1; j >= 0; --j) {
                field[i][j] = field[i][j + nCols];
                field[i][hPad + nCols + hPad - 1 - j] = field[i][hPad + hPad - 1 - j];
            }
        }
    }
}
