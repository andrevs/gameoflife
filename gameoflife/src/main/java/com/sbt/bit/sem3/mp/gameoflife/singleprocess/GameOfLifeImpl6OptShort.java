package com.sbt.bit.sem3.mp.gameoflife.singleprocess;

import com.sbt.bit.sem3.mp.gameoflife.GameOfLife;
import com.sbt.bit.sem3.mp.gameoflife.GameOfLifeConfig;

/**
 * @author Vasyukevich Andrey
 * @since 17.12.2017
 */
public class GameOfLifeImpl6OptShort implements GameOfLife {
    protected final int vPad;
    protected final int hPad;

    public GameOfLifeImpl6OptShort(int vPad, int hPad) {
        this.vPad = vPad;
        this.hPad = hPad;
    }

    @Override
    public int[][] play(GameOfLifeConfig config) {
        short[][] field = expand(config.getNRows(), config.getNCols(), config.getField());

        short[][] nextGenField = new short[vPad + config.getNRows() + vPad][hPad + config.getNCols() + hPad];
        for (int gen = 0; gen < config.getNGens(); ++gen) {
            computeNextGenField(config.getNRows(), config.getNCols(), field, nextGenField);
            short[][] tmp = field;
            field = nextGenField;
            nextGenField = tmp;
        }

        return shrink(config.getNRows(), config.getNCols(), field);
    }

    protected void computeNextGenField(int nRows, int nCols, short[][] field, short[][] nextGenField) {
        for (int i = vPad; i < vPad + nRows; ++i) {
            for (int j = hPad; j < hPad + nCols; ++j) {
                int nc = countNeighbours(i, j, field);
                nextGenField[i][j] = (short)(nc == 3 || field[i][j] == 1 && nc == 2 ? 1 : 0);
            }
        }
        fillPaddings(nRows, nCols, nextGenField);
    }

    protected int countNeighbours(int i, int j, short[][] field) {
        int nc = 0;
        nc += field[i - 1][j - 1] + field[i - 1][j] + field[i - 1][j + 1];
        nc += field[i][j - 1] + field[i][j + 1];
        nc += field[i + 1][j - 1] + field[i + 1][j] + field[i + 1][j + 1];
        return nc;
    }

    protected short[][] expand(int nRows, int nCols, int[][] field) {
        short[][] result = new short[vPad + nRows + vPad][hPad + nCols + hPad];
        for (int i = 0; i < nRows; ++i) {
            for (int j = 0; j < nCols; ++j) {
                result[i + vPad][j + hPad] = (short)field[i][j];
            }
        }
        fillPaddings(nRows, nCols, result);
        return result;
    }

    protected int[][] shrink(int nRows, int nCols, short[][] field) {
        int[][] result = new int[nRows][nCols];
        for (int i = 0; i < nRows; ++i) {
            for (int j = 0; j < nCols; ++j) {
                result[i][j] = field[i + vPad][j + hPad];
            }
        }
        return result;
    }

    protected void fillPaddings(int nRows, int nCols, short[][] field) {
        for (int i = vPad - 1; i >= 0; --i) {
            System.arraycopy(field[i + nRows], hPad, field[i], hPad, nCols);
            System.arraycopy(field[vPad + vPad - 1 - i], hPad, field[vPad + nRows + vPad - 1 - i], hPad, nCols);
        }
        for (int i = 0; i < vPad + nRows + vPad; ++i) {
            for (int j = hPad - 1; j >= 0; --j) {
                field[i][j] = field[i][j + nCols];
                field[i][hPad + nCols + hPad - 1 - j] = field[i][hPad + hPad - 1 - j];
            }
        }
    }
}
