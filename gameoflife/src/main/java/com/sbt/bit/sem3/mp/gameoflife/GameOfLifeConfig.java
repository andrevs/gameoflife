package com.sbt.bit.sem3.mp.gameoflife;

/**
 * @author Vasyukevich Andrey
 * @since 17.12.2017
 */
public class GameOfLifeConfig {
    private final int nRows;
    private final int nCols;
    private final int nGens;
    private final int[][] field;

    public GameOfLifeConfig(int nRows, int nCols, int nGens, int[][] field) {
        this.nRows = nRows;
        this.nCols = nCols;
        this.nGens = nGens;
        this.field = field;
    }

    public int getNRows() {
        return nRows;
    }

    public int getNCols() {
        return nCols;
    }

    public int getNGens() {
        return nGens;
    }

    public int[][] getField() {
        return field;
    }
}
