package com.sbt.bit.sem3.mp.gameoflife.singleprocess;

import com.sbt.bit.sem3.mp.gameoflife.GameOfLife;
import com.sbt.bit.sem3.mp.gameoflife.GameOfLifeConfig;

import java.util.BitSet;

/**
 * @author Vasyukevich Andrey
 * @since 17.12.2017
 */
public class GameOfLifeImpl5OptBitSet implements GameOfLife {
    protected final int vPad;
    protected final int hPad;
    protected final boolean[] willBeAlive;

    public GameOfLifeImpl5OptBitSet(int vPad, int hPad) {
        this.vPad = vPad;
        this.hPad = hPad;

        willBeAlive = new boolean[0x200];
        for (int i = 0; i < 0x200; ++i) {
            boolean cellIsAlive = ((i & 0x10) == 0x10);
            int popcount = Integer.bitCount(i) - (cellIsAlive ? 1 : 0);
            willBeAlive[i] = (popcount == 3 || cellIsAlive && popcount == 2);
        }
    }

    @Override
    public int[][] play(GameOfLifeConfig config) {
        BitSet[] bitSetField = toBitSetField(config.getNRows(), config.getNCols(), config.getField());
        BitSet[] field = expand(config.getNRows(), config.getNCols(), bitSetField);

        BitSet[] nextGenField = new BitSet[vPad + config.getNRows() + vPad];
        for (int i = 0; i < vPad + config.getNRows() + vPad; ++i) {
            nextGenField[i] = new BitSet(hPad + config.getNCols() + hPad);
        }

        for (int gen = 0; gen < config.getNGens(); ++gen) {
            computeNextGenField(config.getNRows(), config.getNCols(), field, nextGenField);
            BitSet[] tmp = field;
            field = nextGenField;
            nextGenField = tmp;
        }

        BitSet[] shrinkedField = shrink(config.getNRows(), config.getNCols(), field);
        int[][] intField = toIntField(config.getNRows(), config.getNCols(), shrinkedField);

        return intField;
    }

    protected void computeNextGenField(int nRows, int nCols, BitSet[] field, BitSet[] nextGenField) {
        for (int i = vPad; i < vPad + nRows; ++i) {
            for (int j = hPad; j < hPad + nCols; ++j) {
                nextGenField[i].set(j, willBeAlive[getMask(i, j, field)]);
            }
        }
        fillPaddings(nRows, nCols, nextGenField);
    }

    protected int getMask(int i, int j, BitSet[] field) {
//        return (field[i - 1].get(j - 1) ? 0x100 : 0) | (field[i - 1].get(j) ? 0x80 : 0) | (field[i - 1].get(j + 1) ? 0x40 : 0)
//                | (field[i].get(j - 1) ? 0x20 : 0) | (field[i].get(j) ? 0x10 : 0) | (field[i].get(j + 1) ? 0x8 : 0)
//                | (field[i + 1].get(j - 1) ? 0x4 : 0) | (field[i + 1].get(j) ? 0x2 : 0) | (field[i + 1].get(j + 1) ? 0x1 : 0);
        int mask = 0;
        for (int di = -1; di <= 1; ++di) {
            for (int dj = -1; dj <= 1; ++dj) {
                mask = (mask << 1) | (field[i + di].get(j + dj) ? 1 : 0);
            }
        }
        return mask;
    }

    protected BitSet[] toBitSetField(int nRows, int nCols, int[][] field) {
        BitSet[] result = new BitSet[nRows];
        for (int i = 0; i < nRows; ++i) {
            result[i] = new BitSet(nCols);
            for (int j = 0; j < nCols; ++j) {
                if (field[i][j] == 1) {
                    result[i].set(j);
                }
            }
        }
        return result;
    }

    protected int[][] toIntField(int nRows, int nCols, BitSet[] field) {
        int[][] result = new int[nRows][nCols];
        for (int i = 0; i < nRows; ++i) {
            for (int j = 0; j < nCols; ++j) {
                if (field[i].get(j)) {
                    result[i][j] = 1;
                }
            }
        }
        return result;
    }

    protected BitSet[] expand(int nRows, int nCols, BitSet[] field) {
        BitSet[] result = new BitSet[vPad + nRows + vPad];
        for (int i = 0; i < vPad + nRows + vPad; ++i) {
            result[i] = new BitSet(hPad + nCols + hPad);
        }
        for (int i = 0; i < nRows; ++i) {
            for (int j = 0; j < nCols; ++j) {
                if (field[i].get(j)) {
                    result[i + vPad].set(j + hPad);
                }
            }
        }
        fillPaddings(nRows, nCols, result);
        return result;
    }

    protected BitSet[] shrink(int nRows, int nCols, BitSet[] field) {
        BitSet[] result = new BitSet[nRows];
        for (int i = 0; i < nRows; ++i) {
            result[i] = new BitSet(nCols);
            for (int j = 0; j < nCols; ++j) {
                if (field[i + vPad].get(j + hPad)) {
                    result[i].set(j);
                }
            }
        }
        return result;
    }

    protected void fillPaddings(int nRows, int nCols, BitSet[] field) {
        for (int i = vPad - 1; i >= 0; --i) {
            for (int j = hPad; j < hPad + nCols; ++j) {
                field[i].set(j, field[i + nRows].get(j));
            }
            for (int j = hPad; j < hPad + nCols; ++j) {
                field[vPad + nRows + vPad - 1 - i].set(j, field[vPad + vPad - 1 - i].get(j));
            }
        }
        for (int i = 0; i < vPad + nRows + vPad; ++i) {
            for (int j = hPad - 1; j >= 0; --j) {
                field[i].set(j, field[i].get(j + nCols));
            }
            for (int j = hPad - 1; j >= 0; --j) {
                field[i].set(hPad + nCols + hPad - 1 - j, field[i].get(hPad + hPad - 1 - j));
            }
        }
    }
}
