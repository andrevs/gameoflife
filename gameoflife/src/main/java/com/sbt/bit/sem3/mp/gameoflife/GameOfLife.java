package com.sbt.bit.sem3.mp.gameoflife;

/**
 * @author Vasyukevich Andrey
 * @since 17.12.2017
 */
public interface GameOfLife {
    int[][] play(GameOfLifeConfig struct);
}
