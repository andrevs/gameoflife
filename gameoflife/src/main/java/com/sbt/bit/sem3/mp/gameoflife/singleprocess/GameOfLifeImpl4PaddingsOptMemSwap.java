package com.sbt.bit.sem3.mp.gameoflife.singleprocess;

import com.sbt.bit.sem3.mp.gameoflife.GameOfLifeConfig;

/**
 * @author Vasyukevich Andrey
 * @since 17.12.2017
 */
public class GameOfLifeImpl4PaddingsOptMemSwap extends GameOfLifeImpl3PaddingsOpt {

    public GameOfLifeImpl4PaddingsOptMemSwap(int vPad, int hPad) {
        super(vPad, hPad);
    }

    @Override
    public int[][] play(GameOfLifeConfig config) {
        int[][] field = expand(config.getNRows(), config.getNCols(), config.getField());

        int[][] nextGenField = new int[vPad + config.getNRows() + vPad][hPad + config.getNCols() + hPad];
        for (int gen = 0; gen < config.getNGens(); ++gen) {
            computeNextGenField(config.getNRows(), config.getNCols(), field, nextGenField);
            int[][] tmp = field;
            field = nextGenField;
            nextGenField = tmp;
        }

        return shrink(config.getNRows(), config.getNCols(), field);
    }

    protected void computeNextGenField(int nRows, int nCols, int[][] field, int[][] nextGenField) {
        for (int i = vPad; i < vPad + nRows; ++i) {
            for (int j = hPad; j < hPad + nCols; ++j) {
                int nc = countNeighbours(i, j, field);
                nextGenField[i][j] = (nc == 3 || field[i][j] == 1 && nc == 2 ? 1 : 0);
            }
        }
        fillPaddings(nRows, nCols, nextGenField);
    }
}
