package com.sbt.bit.sem3.mp.gameoflife.singleprocess;

/**
 * @author Vasyukevich Andrey
 * @since 17.12.2017
 */
public class GameOfLifeImpl3PaddingsOpt extends GameOfLifeImpl2Paddings {

    public GameOfLifeImpl3PaddingsOpt(int vPad, int hPad) {
        super(vPad, hPad);
    }

    protected int countNeighbours(int i, int j, int[][] field) {
        int nc = 0;
        nc += field[i - 1][j - 1] + field[i - 1][j] + field[i - 1][j + 1];
        nc += field[i][j - 1] + field[i][j + 1];
        nc += field[i + 1][j - 1] + field[i + 1][j] + field[i + 1][j + 1];
        return nc;
    }
}
