package com.sbt.bit.sem3.mp.gameoflife.multiprocess;

/**
 * @author Vasyukevich Andrey
 * @since 17.12.2017
 */
public class ResultWrapper {
    public volatile int firstRowInField;
    public volatile int[][] fieldPart;
    public volatile int firstRow;
    public volatile int nRows;

    public ResultWrapper(int firstRowInField, int[][] fieldPart, int firstRow, int nRows) {
        this.firstRowInField = firstRowInField;
        this.fieldPart = fieldPart;
        this.firstRow = firstRow;
        this.nRows = nRows;
    }
}
