package com.sbt.bit.sem3.mp.gameoflife.singleprocess;

import com.sbt.bit.sem3.mp.gameoflife.GameOfLife;
import com.sbt.bit.sem3.mp.gameoflife.GameOfLifeConfig;

/**
 * @author Vasyukevich Andrey
 * @since 17.12.2017
 */
public class GameOfLifeImpl2Paddings implements GameOfLife {
    protected final int vPad;
    protected final int hPad;

    public GameOfLifeImpl2Paddings(int vPad, int hPad) {
        this.vPad = vPad;
        this.hPad = hPad;
    }

    @Override
    public int[][] play(GameOfLifeConfig config) {
        int[][] field = expand(config.getNRows(), config.getNCols(), config.getField());

        for (int gen = 0; gen < config.getNGens(); ++gen) {
            field = computeNextGenField(config.getNRows(), config.getNCols(), field);
        }

        return shrink(config.getNRows(), config.getNCols(), field);
    }

    protected int[][] computeNextGenField(int nRows, int nCols, int[][] field) {
        int[][] nextGenField = new int[vPad + nRows + vPad][hPad + nCols + hPad];
        for (int i = vPad; i < vPad + nRows; ++i) {
            for (int j = hPad; j < hPad + nCols; ++j) {
                int nc = countNeighbours(i, j, field);
                nextGenField[i][j] = (nc == 3 || field[i][j] == 1 && nc == 2 ? 1 : 0);
            }
        }
        fillPaddings(nRows, nCols, nextGenField);
        return nextGenField;
    }

    protected int countNeighbours(int i, int j, int[][] field) {
        int nc = 0;
        int[] d = {-1, 0, 1};
        for (int di : d) {
            for (int dj : d) {
                if (di != 0 || dj != 0) {
                    nc += field[i + di][j + dj];
                }
            }
        }
        return nc;
    }

    protected int[][] expand(int nRows, int nCols, int[][] field) {
        int[][] result = new int[vPad + nRows + vPad][hPad + nCols + hPad];
        for (int i = 0; i < nRows; ++i) {
            System.arraycopy(field[i], 0, result[i + vPad], hPad, nCols);
        }
        fillPaddings(nRows, nCols, result);
        return result;
    }

    protected int[][] shrink(int nRows, int nCols, int[][] field) {
        int[][] result = new int[nRows][nCols];
        for (int i = 0; i < nRows; ++i) {
            System.arraycopy(field[i + vPad], hPad, result[i], 0, nCols);
        }
        return result;
    }

    protected void fillPaddings(int nRows, int nCols, int[][] field) {
        for (int i = vPad - 1; i >= 0; --i) {
            System.arraycopy(field[i + nRows], hPad, field[i], hPad, nCols);
            System.arraycopy(field[vPad + vPad - 1 - i], hPad, field[vPad + nRows + vPad - 1 - i], hPad, nCols);
        }
        for (int i = 0; i < vPad + nRows + vPad; ++i) {
            for (int j = hPad - 1; j >= 0; --j) {
                field[i][j] = field[i][j + nCols];
                field[i][hPad + nCols + hPad - 1 - j] = field[i][hPad + hPad - 1 - j];
            }
        }
    }
}
